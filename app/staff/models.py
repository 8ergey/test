from django.db import models
from django.db.models.functions import Coalesce
from django.utils.functional import cached_property

from common.models import BaseModel, AmountField


class Department(BaseModel):
    name = models.CharField("Name", max_length=32, db_index=True)
    manager = models.OneToOneField(
        "staff.Employee",
        verbose_name="Head of department",
        related_name="my_department",
        on_delete=models.SET_NULL,
        null=True, blank=True
    )

    def __str__(self):
        return self.name

    @cached_property
    def employees_count(self):
        return self.employees.count()

    @cached_property
    def total_salary(self):
        return (
            self.employees.aggregate(
                total_salary=Coalesce(
                    models.Sum("salary"), 0,
                    output_field=models.DecimalField())
            )["total_salary"]
        )

    class Meta:
        verbose_name = "department"
        verbose_name_plural = "departments"
        db_table = "departments"


class Position(BaseModel):
    name = models.CharField("Name", max_length=32, db_index=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "position"
        verbose_name_plural = "positions"
        db_table = "positions"


class Employee(BaseModel):
    # Personal
    first_name = models.CharField("First Name", max_length=32)
    middle_name = models.CharField("Middle Name", max_length=32, null=True, blank=True)
    last_name = models.CharField("Last Name", max_length=32, db_index=True)
    birth_date = models.DateField("DOB", null=True, blank=True)
    photo = models.ImageField("Photo", null=True, blank=True)

    # Work
    department = models.ForeignKey(
        Department, verbose_name="Department",
        on_delete=models.SET_NULL,
        related_name="employees",
        null=True, blank=True
    )
    position = models.ForeignKey(
        Position, verbose_name="Position",
        on_delete=models.SET_NULL,
        related_name="employees",
        null=True, blank=True
    )
    salary = AmountField("Salary")

    def get_short_name(self):
        return f"{self.first_name} {self.last_name}"

    def __str__(self):
        return f"{self.get_short_name()} ({self.department})"

    class Meta:
        verbose_name = "employee"
        verbose_name_plural = "employees"
        db_table = "employees"
        ordering = ["last_name", "first_name"]
