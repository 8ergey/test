## Quick Start

**NOTE**: The project uses Python 3.9, so need it installed first. It is recommended to use [`pyenv`](https://github.com/pyenv/pyenv) for installation.

Here is a short instruction on how to quickly set up the project for development:

1. Install [`poetry`](https://python-poetry.org/)
2. `git clone git@gitlab.com:8ergey/test.git`
3. Install requierements:

```bash
$ poetry install
$ poetry shell
```

4. Apply migrations: `$ python manage.py migrate`
5. Manually create a superuser: `$ python manage.py createsuperuser --username admin --email admin@admin.com`
6. Load initial data: `$ python manage.py loaddata staff.json`
7. Run the server: `$ python manage.py runserver`

