from django.contrib import admin
from django.contrib.admin.utils import flatten_fieldsets


class BaseModelAdmin(admin.ModelAdmin):
    search_fields = ("id",)

    def save_model(self, request, obj, form, change):
        if getattr(obj, "created_by", None) is None:
            obj.created_by = request.user
        obj.save()

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for instance in instances:
            if not hasattr(instance, "created_by"):
                instance.created_by = request.user
            instance.save()
        formset.save_m2m()

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = super().get_readonly_fields(request, obj)
        if "created_by" not in readonly_fields:
            return *readonly_fields, "created_by", "created_dttm"
        return readonly_fields

    def get_fieldsets(self, request, obj=None):
        fieldsets = super().get_fieldsets(request, obj)
        flat_fieldsets = flatten_fieldsets(fieldsets)
        if "created_by" not in flat_fieldsets:
            return (
                *fieldsets,
                ("Technical Record", {"fields": ("created_by", "created_dttm")}),
            )
        return fieldsets
