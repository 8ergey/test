from collections import OrderedDict

from rest_framework import serializers


class BaseModelSerializer(serializers.ModelSerializer):
    """
    A base ModelSerializer used by default in the project. Extends default ModelSerializer by:
    * allowing the fields to be defined on a per-view/request basis,
      fields can be whitelisted, blacklisted.
    * remove null fields from representation
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if (
            getattr(self.Meta, "fields", None) is None
            and getattr(self.Meta, "exclude", None) is None
        ):
            setattr(self.Meta, "fields", "__all__")

    def create(self, validated_data):
        validated_data["created_by"] = self.context["request"].user
        return super().create(validated_data)

    def to_representation(self, instance):
        result = super(BaseModelSerializer, self).to_representation(instance)
        return OrderedDict([(key, result[key]) for key in result if result[key] is not None])
