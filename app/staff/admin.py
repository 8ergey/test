from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

# Register your models here.
from common.admin import BaseModelAdmin
from staff.models import Employee, Department, Position

#
# class EmployeeInline(admin.StackedInline):
#     model = Employee
#     can_delete = False
#     verbose_name_plural = "employee"
#     fk_name = "user"
#
#
# class UserAdmin(BaseModelAdmin):
#     list_display = (
#         "first_name",
#         "middle_name",
#         "last_name",
#         "phone",
#         "email",
#         "is_active",
#     )
#
#     search_fields = (
#         "first_name",
#         "middle_name",
#         "last_name",
#         "phone",
#         "email",
#     )
#
#     fieldsets = (
#         (
#             "Personal",
#             {
#                 "fields": (
#                     "password",
#                     "first_name",
#                     "middle_name",
#                     "last_name",
#                     "birth_date",
#                     "photo",
#                     "email",
#                     "phone",
#                 )
#             },
#         ),
#         ("Important dates", {"fields": ("last_login", "date_joined")}),
#     )
#     add_fieldsets = (
#         (
#             None,
#             {
#                 "classes": ("wide",),
#                 "fields": (
#                     "first_name",
#                     "last_name",
#                     "password1",
#                     "password2",
#                 ),
#             },
#         ),
#     )
#     readonly_fields = ("last_login", "date_joined")
#
#     inlines = (EmployeeInline,)
#
#     def save_formset(self, request, form, formset, change):
#         instances = formset.save(commit=False)
#         for obj in formset.deleted_objects:
#             obj.delete()
#         for instance in instances:
#             if not hasattr(instance, "created_by"):
#                 instance.created_by = request.user
#             instance.save()
#         formset.save_m2m()


class PositionModelAdmin(BaseModelAdmin):
    pass


class DepartmentModelAdmin(BaseModelAdmin):
    list_display = (
        "name",
        "manager",
        "employees",
        "total_salary",
    )

    list_select_related = ("manager",)

    search_fields = (
        "name",
    )

    # --- Computed fields ----
    def employees(self, obj):
        return obj.employees_count

    employees.short_description = "Number of employees"

    def total_salary(self, obj):
        return obj.total_salary

    total_salary.short_description = "Employees total salary"


class EmployeeModelAdmin(BaseModelAdmin):
    list_display = (
        "name",
        "department",
        "position",
        "salary"
    )

    list_select_related = ("department", "position")

    list_filter = (
        "department",
        "position"
    )

    search_fields = (
        "first_name",
        "last_name",
        "middle_name",
    )

    # --- Computed fields ----
    def name(self, obj):
        return obj.get_short_name()

    name.short_description = "Name"
    name.admin_order_field = "last_name"


admin.site.register(Position, PositionModelAdmin)
admin.site.register(Department, DepartmentModelAdmin)
admin.site.register(Employee, EmployeeModelAdmin)

