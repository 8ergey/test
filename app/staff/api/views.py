from djangorestframework_camel_case.parser import CamelCaseMultiPartParser, CamelCaseFormParser
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, viewsets, filters
from rest_framework.permissions import AllowAny

from staff.api.serializers import DepartmentSerializer, EmployeeSerializer
from staff.models import Department, Employee


class DepartmentViewSet(
    generics.ListAPIView,
    viewsets.GenericViewSet
):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
    pagination_class = None
    permission_classes = (AllowAny,)


class EmployeeViewSet(
    generics.ListAPIView,
    generics.CreateAPIView,
    generics.DestroyAPIView,
    viewsets.GenericViewSet
):
    yasg_parser_classes = [CamelCaseFormParser, CamelCaseMultiPartParser]
    queryset = Employee.objects.select_related("position", "department")
    serializer_class = EmployeeSerializer
    filter_backends = (
        filters.SearchFilter,
        DjangoFilterBackend
    )
    filterset_fields = ("department_id",)
    search_fields = (
        "last_name",
    )
