from django.conf import settings
from django.db import models
from django.db.models import DecimalField


class BaseModel(models.Model):
    created_dttm = models.DateTimeField("Created At", auto_now_add=True)
    created_by = models.ForeignKey(
        to=settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
        editable=False,
        verbose_name="Created By",
        related_name="created_%(class)ss",
        related_query_name="%(app_label)s_%(class)ss",
    )

    class Meta:
        abstract = True


class AmountField(DecimalField):
    def __init__(
        self, verbose_name=None, name=None, max_digits=10, decimal_places=2, **kwargs
    ):
        super().__init__(verbose_name, name, max_digits, decimal_places, **kwargs)
