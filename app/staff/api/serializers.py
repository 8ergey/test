from common.serializers import BaseModelSerializer
from staff.models import Employee, Position, Department


class PositionSerializer(BaseModelSerializer):
    class Meta:
        model = Position


class DepartmentSerializer(BaseModelSerializer):
    class Meta:
        model = Department
        fields = (
            "id",
            "name",
            "employees_count",
            "total_salary"
        )


class EmployeeSerializer(BaseModelSerializer):
    class Meta:
        model = Employee

    def to_representation(self, instance):
        representation = super(EmployeeSerializer, self).to_representation(instance)
        representation["position"] = PositionSerializer(instance.position).data
        representation["department"] = PositionSerializer(instance.department).data
        return representation
