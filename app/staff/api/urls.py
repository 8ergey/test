from rest_framework import routers

from staff.api import views

router = routers.SimpleRouter()


router.register(r"departments", views.DepartmentViewSet, basename="departments")
router.register(r"employees", views.EmployeeViewSet, basename="employees")

urlpatterns = router.urls
